﻿using SeohyunMVC.App_Start;
using System;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Net.Http.Headers;
using System.Web.Http;
using SeohyunMVC.DAL;
using NLog.Common;
using Hangfire;
using System.Linq;
using System.Web;

namespace SeohyunMVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            System.Web.Http.GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            var databaseContext = new DatabaseContext();
            databaseContext.Database.Initialize(true);
            string nlogPath = Server.MapPath("nlog-web.log");
            InternalLogger.LogFile = nlogPath;
            InternalLogger.LogLevel = NLog.LogLevel.Trace;
         

        }
        protected void Application_End(object sender, EventArgs e)
        {
           // HangfireBootstrapper.Instance.Stop();
        }

      
    }
}
