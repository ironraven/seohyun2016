﻿using System.Collections.Generic;
using KalikoCMS.Core;
using SeohyunMVC.PageTypes;
using System.Web.WebPages;

namespace SeohyunMVC.Models.ViewModels
{
    public class StartPageViewModel : IPageViewModel<HomePageType>
    {
        public StartPageViewModel(HomePageType currentPage)
        {
            CurrentPage = currentPage;
        }

        public HomePageType CurrentPage { get; private set; }

        public IEnumerable<CmsPage> TopMenu { get; set; }
        public IEnumerable<ArticlePageType> Childrenpages { get; set; }
        //public IEnumerable<SitePage> LatestNews { get; internal set; }
    }
}