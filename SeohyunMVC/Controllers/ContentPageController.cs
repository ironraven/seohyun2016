﻿using Kaliko.ImageLibrary;
using Kaliko.ImageLibrary.Scaling;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Mvc.Framework;
using SeohyunMVC.DAL;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace SeohyunMVC.Controllers
{
    //[RequireHttps]
    public class ContentPageController : PageController<ContentPageType>
    {

        [HttpGet]
        public override ActionResult Index(ContentPageType currentPage)
        {

            var model = ContentPageViewModelBuilder.Create(currentPage);
           
            model.Childrenpages = GetNewsHolders(currentPage);
            foreach (var page in model.Childrenpages)
            {
                if (!string.IsNullOrEmpty(page.TopImage.ImageUrl))
                {
                    string Image = page.TopImage.ImageUrl.Split('/').Last();
                    var lastPart = Image.LastIndexOf(".");
                    var image = Image.Substring(0, lastPart);
                    //var Exists = string.Format("http://{0}:45801/ImageCache/cropped/{1}", Request.Url.Host, page.PageId + ".jpg");
                    if (!System.IO.File.Exists(string.Format("/ImageCache/cropped/{0}.jpg", image)))
                    {
                        var image2Init = HttpContext.Server.MapPath(page.TopImage.OriginalImageUrl);
                        var NewImage = new KalikoImage(image2Init);
                        NewImage = NewImage.Scale(new CropScaling(650, 350));
                        string path = HttpContext.Server.MapPath("~/ImageCache/cropped");
                        path = string.Format("{0}\\{1}{2}", path, image, ".jpg");
                        NewImage.SaveJpg(@path, 65);

                        //page.ThumbImage.Value = string.Format("/ImageCache/cropped/{0}.jpg", image);
                    }
                }
            }

            return View(model);
        }


        private static IEnumerable<ContentPageType> GetNewsHolders(ContentPageType currentPage)
        {
            // Get the page type for news lists
            var pageType = KalikoCMS.Core.PageType.GetPageType(typeof(ContentPageType));

            // Get all children from the news root
            return PageFactory.GetChildrenForPageOfPageType(currentPage.RootId, pageType.PageTypeId).Select(x => x.ConvertToTypedPage<ContentPageType>());
        }

    }
}
