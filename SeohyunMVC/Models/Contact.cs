﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models
{
    [Serializable]
    public class Contact
    {
        [Key]
        public int Id { get; set; }

        [Display(Prompt = "Your Name")]
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        public string Telephone { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [DataType(DataType.MultilineText)]
        public string Message { get; set; }
        [UIHint("DateTime")]
        [DataType(DataType.DateTime)]
        public DateTime? Datesumbitted { get; set; }

        public string IP { get; set; }

        // [Departments]
        //  [UIHint("DropDown")]
        //  public int? Department { get; set; }
    }

    //public class DepartmentsAttribute : Attribute, IMetadataAware
    //{
    //    public void OnMetadataCreated(ModelMetadata metadata)
    //    {
    //        metadata.TemplateHint = "Dropdown";
    //        metadata.Model = new SelectList(
    //          GetDepartments(),
    //          "ID",
    //          "Name",
    //          metadata.Model
    //        );
    //    }

    //private Department[] GetDepartments()
    //{

    //    // IEnumerable<Department[]> result;

    //    using (var db = new DatabaseContext())
    //    {
    //        var result = db.ContactDepartMents.SqlQuery("Select KeyName as ID ,Value as Name from Department order by KeyName asc").ToArray();
    //        return result;
    //        //  result = connection.Query<Department>("select rowID as ID, TItle as Name from cms_DC_ContactDepartments where langId=@languageID and IsActive=1 order by ordering ASC", new { languageID }).ToArray();
    //    }

    //}

    //}


    [Serializable]
    public class Department
    {
        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

    }

}