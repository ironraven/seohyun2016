﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models
{
    [Serializable]
    public class Cms_logs
    {
        [Key]
        public int Id { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime EventDateTime { get; set; }
        [MaxLength(100)]
        public string EventLevel { get; set; }
        [MaxLength(100)]
        public string UserName { get; set; }
        [MaxLength(100)]
        public string MachineName { get; set; }
        public string EventMessage { get; set; }
        [MaxLength(100)]
        public string ErrorSource { get; set; }
        [MaxLength(500)]
        public string ErrorClass { get; set; }
        public string ErrorMethod { get; set; }
        public string ErrorMessage { get; set; }
        public string InnerErrorMessage { get; set; }
    }
}

