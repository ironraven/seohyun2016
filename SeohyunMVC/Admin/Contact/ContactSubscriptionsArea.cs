﻿using KalikoCMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Admin.Contact
{
    public class ContactSubscriptionsArea : IDashboardArea
    {
        public string Title { get { return "Contact Subscriptions"; } }
        public string MenuName { get { return "Subscriptions"; } }
        public string Icon { get { return "icon-users"; } }
        public string Path
        {
            get { return "Contact"; }

        }
    }
}