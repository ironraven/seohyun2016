﻿<%@ Page Language="C#" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="CreateSnippet.aspx.cs" Inherits="SeohyunMVC.Admin.Snippets.CreateSnippet" MasterPageFile="../Templates/MasterPages/Admin.Master" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <link rel="stylesheet" href="/content/bootstrap.min.css" />
    <link rel="stylesheet" href="/Admin/Assets/themes/dakota/theme.min.css" />
    <script src="/scripts/Moment.js"></script>
    <script src="/Admin/Assets/js/kalikocms.admin.js?v=27665443"></script>
    <script src="/Admin/Assets/js/kalikocms.admin.editor.min.js?v=27665443" type="text/javascript"></script>
    <script src="/Admin/Assets/js/kalikocms.admin.core.min.js?v=27665443"></script>
  
    <script type="text/javascript">
        $(document).ready(function () {
            initHtmlEditor('../assets/');
            initMarkdownEditor();
         
            warnBeforeLeavingIfChangesBeenMade();

            $('#versionbutton').click(function () {
                parent.openModal("Content/Dialogs/PageVersionDialog.aspx?id=9ce50cbf-193c-447a-b72d-5d89dd9788f6", 700, 500);
            });

            $('#advanced-options').click(function () {
                $('#MainContent_AdvancedOptionButton').hide();
                $('#advanced-panel').slideDown();
            });
        });

    </script>

    <div class="admin-page">
        <div class="page-head">
            <h1>Create Snippet</h1>
        </div>

        <div class="main-content">
            <div class="inner-container form-horizontal">
                <% SeohyunMVC.Helpers.MVCUtility.RenderAction("Snippets", "Index", new {}); %>
                <div class="form-buttons">
                    <asp:LinkButton ID="SaveButton" CssClass="btn btn-primary" runat="server" OnClick="SaveButton_click">
                        <i class="icon-thumbs-up"></i> Create Snippet
                    </asp:LinkButton>
                    <a href="Snippets" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

