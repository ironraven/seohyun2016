﻿using KalikoCMS.Attributes;
using KalikoCMS.PropertyType;
using KalikoCMS.Core;
using KalikoCMS.Admin.Templates;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Settings
{
    [SiteSettings(AllowedTypes = new[] { typeof(ArticleList), typeof(HomePageType), typeof(ArticlePageType) })]
    public class Site
    {
        [Property("Company")]
        public virtual StringProperty CompanyName { get; set; }

        [Property("Show social links")]
        public virtual BooleanProperty ShowSocialLinks { get; set; }
    }
}