﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;
using System.Collections.Generic;

namespace SeohyunMVC.PageTypes.PropertyTypes
{
    [PropertyType("5033A828-B49A-4A19-9C20-0F9BEBBD3273", "NewsItem", "NewsItem", EditorControl)]
    public class HomeArticlesProperty : CompositeProperty
    {
        [Property("Promo Article")]
        public PageLinkProperty articles {get;set;}
       // public virtual KalikoCMS.Core.CmsPage {get;set;}
    }
}
