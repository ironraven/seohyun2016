﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models
{
    [Serializable]
    public class SEOCanonical
    {
        public string Url { get; set; }
    }
    public class SEODescription
    {
        public string Text { get; set; }
    }
}