﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SeohyunMVC.Admin.Contact.Default" MasterPageFile="../Templates/MasterPages/Admin.Master" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <link rel="stylesheet" href="/content/bootstrap.min.css" />
    <link rel="stylesheet" href="/Admin/Assets/themes/dakota/theme.min.css" />
    <link rel="stylesheet" href="/content/bootstrap-datetimepicker.min.css" />

    <div class="admin-page">

        <div class="page-head">
            <h1>Manage Text/HTML Snippets</h1>
        </div>

        <div class="main-content">
            <div class="inner-container" style="max-width: 1280px;">
                <% SeohyunMVC.Helpers.MVCUtility.RenderAction("Snippets", "List", new { }); %>
                <a href="Snippets/CreateSnippet.aspx" class="btn btn-primary pull-right"><i class="icon-plus"></i> Add Snippet</a>
            </div>
        </div>

    </div>
</asp:Content>
