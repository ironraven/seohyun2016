﻿using System.Collections.Generic;
using KalikoCMS.Core;
using SeohyunMVC.PageTypes;
using System.Web.WebPages;

namespace SeohyunMVC.Models.ViewModels
{
    public class ContentPageViewModel : IPageViewModel<ContentPageType>
    {
        public ContentPageViewModel(ContentPageType currentPage)
        {
            CurrentPage = currentPage;
        }
        public ContentPageType CurrentPage { get; private set; }
        public IEnumerable<ContentPageType> Childrenpages { get; set; }
        public IEnumerable<CmsPage> TopMenu { get; set; }
    }
}


