﻿using KalikoCMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Admin.Register
{
    public class ExceptionsArea : IDashboardArea
    {
        public string Title { get { return "Cms Exceptions"; } }
        public string MenuName { get { return "Exceptions"; } }
        public string Icon { get { return "icon-file"; } }
        public string Path { get { return "Exceptions"; } }
    }
}


