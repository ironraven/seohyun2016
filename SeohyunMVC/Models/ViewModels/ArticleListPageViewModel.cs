﻿using KalikoCMS.Core;
using PagedList;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models.ViewModels
{
    public class ArticleListPageViewModel : IPageViewModel<ArticleList>
    {
        public ArticleListPageViewModel(ArticleList currentPage)
        {
            CurrentPage = currentPage;
        }
        public ArticleList CurrentPage { get; private set; }
        public IEnumerable<CmsPage> TopMenu { get; set; }
        public IPagedList<ArticlePageType> News { get; set; }
        public IEnumerable<ArticleList> NewsHolders { get; set; }
        public int Page { get; set; }
    }
}