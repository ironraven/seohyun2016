//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SeohyunMVC.DAL
{
    using System;
    using System.Collections.Generic;
    
    public partial class Page
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Page()
        {
            this.PageInstances = new HashSet<PageInstance>();
            this.PageProperties = new HashSet<PageProperty>();
        }
    
        public System.Guid PageId { get; set; }
        public System.Guid ParentId { get; set; }
        public System.Guid RootId { get; set; }
        public int TreeLevel { get; set; }
        public int SortOrder { get; set; }
        public int PageTypeId { get; set; }
    
        public virtual PageType PageType { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PageInstance> PageInstances { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PageProperty> PageProperties { get; set; }
    }
}
