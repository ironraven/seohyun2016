﻿using System.Collections.Generic;
using System.Linq;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.ViewModels
{
    public class ContactPageViewModelBuilder
    {
        public static ContactPageViewModel Create(ContactPageType currentPage)
        {
            var model = new ContactPageViewModel(currentPage);
            PageViewModelBuilder.SetBaseProperties(model);
            return model;
        }
    }
}


