﻿using NLog;
using SeohyunMVC.DAL;
using SeohyunMVC.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeohyunMVC.Services
{
    public class UtilitiesService
    {
        private SnippetsRepository _snippetsRepo;
        private Logger _log;

        public UtilitiesService() { }

        public UtilitiesService(SnippetsRepository snippetrepo)
        {
            _log = LogManager.GetCurrentClassLogger();
            _snippetsRepo = snippetrepo;

        }


        public static string GetSnippet(string snippet, string snippetContent)
        {
            UtilitiesService Util = new UtilitiesService();

            return Util.RetrieveSnippet(snippet, snippetContent);
        }


        public static string GetSnippet(string snippet)
        {
            UtilitiesService Util = new UtilitiesService();

            return  Util.RetrieveSnippet(snippet, null);
        }


        private string RetrieveSnippet(string snippet, string snippetContent)
        {
            var snippetsRepos = DependencyResolver.Current.GetService<SnippetsRepository>();
            var SnippetFound = snippetsRepos.AllIncluding().Where(x => x.Title == snippet).FirstOrDefault();
            if (SnippetFound == null)
            {
                using (var db = new DatabaseContext())
                {
                    var item = new Models.Snippet()
                    {
                        Title = snippet,
                        Area = snippetContent,
                        Active = true
                    };

                    db.Snippets.Add(item);
                    db.SaveChanges();
                }
                SnippetFound = snippetsRepos.AllIncluding().Where(x => x.Title == snippet).FirstOrDefault();

            }
            return SnippetFound.Area;
            //return new SnippetViewModel()
            //{
            //    Content = SnippetFound.Area
            //};
        }
    }
}