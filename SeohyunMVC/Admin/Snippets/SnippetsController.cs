﻿using KalikoCMS.PropertyType;
using NLog;
using SeohyunMVC.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SeohyunMVC.Admin.Contact
{
    public class SnippetsController : Controller
    {
        private readonly DatabaseContext _database = new DatabaseContext();
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        public IEnumerable<Models.Snippet> Snippets;


        public ActionResult Delete(int? id)
        {
            ViewData["SaveSuccess"] = false;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Snippet contact = _database.Snippets.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: /Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewData["SaveSuccess"] = true;
            Models.Snippet snippet = _database.Snippets.Find(id);
            _database.Snippets.Remove(snippet);
            _database.SaveChanges();
           return View(snippet);
        }


        public ActionResult Details(int? id)
        {
            ViewData["SaveSuccess"] = false;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Snippet snippet = _database.Snippets.Find(id);
            if (snippet == null)
            {
                return HttpNotFound();
            }
            return View(snippet);
        }


        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            Models.Snippet snippet = _database.Snippets.Find(id);
            if (snippet == null)
            {
                return HttpNotFound();
            }
            return View(snippet);
        }
        [HttpPost]
        public ActionResult Edit(Models.Snippet snippet)
        {
            ViewData["SaveSuccess"] = false;
            if (ModelState.IsValid)
            {
               
                _database.Entry(snippet).State = EntityState.Modified;
                _database.SaveChanges();
                ViewData["SaveSuccess"] = true;
                Redirect("/Admin/Snippets");
                // return RedirectToAction("Index");
              
            }
            return View(snippet);
        }


        [HttpGet]
        public ActionResult List()
        {
            using (var repo = new SnippetsRepository())
            {
                Snippets = repo.All.OrderByDescending(x => x.Id).ToArray();
            }

            return View(Snippets);
        }


        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(Models.Snippet model)
        {
            ViewData["SaveSuccess"] = false;
            //  if (ModelState.IsValid)
            //{
        

            // ViewData["SaveSuccess"] = true;
            if (Save(model))
            {
                ViewData["SaveSuccess"] = true;
                ViewData["Nothing"] = false;

            }
            else
            {
                ViewData["SaveSuccess"] = false;
                ViewData["Nothing"] = true;
            }

            // }
            return View();
        }
        private bool Save(Models.Snippet model)
        {
            try
            {
                return SaveIntoDb(model);


            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Something went Wrong Saving the Contact:" + e.ToString());
                return false;
            }
        }



        private static bool SaveIntoDb(Models.Snippet model)
        {

            using (var db = new DatabaseContext())
            {
                db.Snippets.Add(model);


                db.SaveChanges();
            }


            return true;
        }


        public void Create(string area, string Title , bool active)
        {
            using (var db = new DatabaseContext())
            {
                db.Snippets.Add(new Models.Snippet
                {
                   Active = active,
                   Area = area,
                   Title = Title,
                 

                });

                db.SaveChanges();
                Redirect("/Admin/Snippets");
            }
        }
    }
}