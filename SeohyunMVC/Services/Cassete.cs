﻿using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Services
{
    public class Cassete : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {

            bundles.Add<StylesheetBundle>("Content", bundle => bundle.EmbedImages());
            bundles.AddPerSubDirectory<ScriptBundle>("Scripts");
        }
    }
}