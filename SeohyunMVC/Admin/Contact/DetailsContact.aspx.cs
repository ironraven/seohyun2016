﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeohyunMVC.Admin.Contact
{
    public partial class DetailsContact : System.Web.Mvc.ViewPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void SaveButton_click(object sender, EventArgs e)
        {
            var id = Request.QueryString["id"];
            if (!string.IsNullOrEmpty(id))
            {
                Response.Redirect("/Admin/Contact/EditContact.aspx?id="+ id);
                //Server.Transfer("EditContact.aspx?id="+ id, false);
            }


            if (IsPostBack && Page.IsValid)
            {
               
            }
            else
            {

            }
        }
    }
}