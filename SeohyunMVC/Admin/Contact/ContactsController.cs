﻿using NLog;
using SeohyunMVC.DAL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SeohyunMVC.Admin.Contact
{
    public class ContactsController : Controller
    {
        private readonly DatabaseContext _database = new DatabaseContext();
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        public IEnumerable<Models.Contact> Contacts;


        public ActionResult Delete(int? id)
        {
            ViewData["SaveSuccess"] = false;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Contact contact = _database.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }

        // POST: /Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ViewData["SaveSuccess"] = true;
            Models.Contact contact = _database.Contacts.Find(id);
            _database.Contacts.Remove(contact);
            _database.SaveChanges();
           return View(contact);
        }


        public ActionResult Details(int? id)
        {
            ViewData["SaveSuccess"] = false;
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Models.Contact contact = _database.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return View(contact);
        }


        [HttpGet]
        public ActionResult Edit(int id = 0)
        {
            Models.Contact Contact = _database.Contacts.Find(id);
            if (Contact == null)
            {
                return HttpNotFound();
            }
            return View(Contact);
        }
        [HttpPost]
        public ActionResult Edit(Models.Contact contact)
        {
            ViewData["SaveSuccess"] = false;
            if (ModelState.IsValid)
            {
                contact.Datesumbitted = Convert.ToDateTime(contact.Datesumbitted);
                _database.Entry(contact).State = EntityState.Modified;
                _database.SaveChanges();
                ViewData["SaveSuccess"] = true;
                Redirect("/Admin/Contact");
                // return RedirectToAction("Index");
              
            }
            return View(contact);
        }


        [HttpGet]
        public ActionResult List()
        {
            using (var repo = new ContactRepository())
            {
                Contacts = repo.All.OrderByDescending(x => x.Datesumbitted.Value).ToArray();
            }

            return View(Contacts);
        }


        [HttpGet]
        public ActionResult Index()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Index(Models.Contact model)
        {
            ViewData["SaveSuccess"] = false;
            //  if (ModelState.IsValid)
            //{
            model.IP = Request.UserHostAddress;

            // ViewData["SaveSuccess"] = true;
            if (Save(model))
            {
                ViewData["SaveSuccess"] = true;
                ViewData["Nothing"] = false;

            }
            else
            {
                ViewData["SaveSuccess"] = false;
                ViewData["Nothing"] = true;
            }

            // }
            return View();
        }
        private bool Save(Models.Contact model)
        {
            try
            {
                return SaveIntoDb(model);


            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Something went Wrong Saving the Contact:" + e.ToString());
                return false;
            }
        }



        private static bool SaveIntoDb(Models.Contact model)
        {

            using (var db = new DatabaseContext())
            {
                db.Contacts.Add(model);


                db.SaveChanges();
            }


            return true;
        }


        public void Create(string Name, string Surname, string Email, string Message, string Telephone, string IP)
        {
            using (var db = new DatabaseContext())
            {
                db.Contacts.Add(new Models.Contact
                {
                    Name = Name,
                    Surname = Surname,
                    Email = Email,
                    Message = Message,
                    // Department = model.Department,
                    Telephone = Telephone,
                    Datesumbitted = DateTime.Now,
                    IP = IP
                });

                db.SaveChanges();
                Redirect("/Admin/Contact");
            }
        }
    }
}