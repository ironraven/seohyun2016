﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using KalikoCMS.Search;
using SeohyunMVC.Business.PropertyTypes;
using SeohyunMVC.PageTypes.PropertyTypes;

namespace SeohyunMVC.PageTypes
{
    /// <summary>
    /// This is the type for our start page. Here we'll use our own custom property type; FeatureProperty
    /// </summary>
    [PageType("StartPage", "Start page", PageTypeDescription = "Used for start page", PreviewImage = "/Assets/Images/startpage.png")]
    public class HomePageType: SitePage, IIndexable
    {
        /// <summary>
        /// A collection of 0..n of our custom FeatureProperty type
        /// </summary>
        [Property("Main feature slides", TabGroup = "Slides")]
        public virtual CollectionProperty<FeatureProperty> Slides { get; set; }


        [Property("Featured links", TabGroup = "Links")]
        public virtual CollectionProperty<PromoPagesProperty> PromoPages { get; set; }


        [Property("Featured Articles", TabGroup = "Articles")]
        public virtual CollectionProperty<HomeArticlesProperty> HomeArticles { get; set; }

        [Property("Main feature", TabGroup = "Feautures")]
        public virtual FeatureProperty MainFeature { get; set; }


        [Property("Secondary feature", TabGroup = "Feautures")]
        public virtual FeatureProperty SecondaryFeature { get; set; }

        public IndexItem MakeIndexItem(CmsPage page)
        {
            // We start by casting the generic CmsPage object to our page type
            var typedPage = page.ConvertToTypedPage<HomePageType>();

            // Get the base index item with basic information already set
            var indexItem = typedPage.GetBaseIndexItem();

            // Add additional information to index, this is where you add the page's properties that should be searchable
            indexItem.Title = typedPage.PageName;
            indexItem.Summary = typedPage.MainFeature.Description.Value;
            indexItem.Content = typedPage.SecondaryFeature.Description.Value ;
         
          //  indexItem.Tags = typedPage.Tags.ToString();

            // We set a category in order to be able to single out search hits
            indexItem.Category = "Home";

            return indexItem;
        }
    }
}