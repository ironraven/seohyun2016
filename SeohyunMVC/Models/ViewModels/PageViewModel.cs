﻿using KalikoCMS.Core;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models.ViewModels
{
    public class PageViewModel<T> : IPageViewModel<T> where T : SitePage 
    {
        public PageViewModel(T currentPage)
        {
            CurrentPage = currentPage;
            TopMenu = new List<CmsPage>().ToArray();

        }

        public T CurrentPage { get; private set; }
        public IEnumerable<CmsPage> TopMenu { get; set; }
    }
}