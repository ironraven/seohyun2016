﻿using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Data;
using KalikoCMS.Identity.Register;
using AspNet.Identity.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SeohyunMVC.Admin.Snippets;

namespace SeohyunMVC.Admin.Register
{
    public class ExceptionsStartUp : IStartupSequence
    {
        public void Startup()
        {
            var area = new ContactSubscriptionArea();
          
            Dashboard.RegisterArea(area);
          
            AspNet.Identity.DataAccess.Data.DataContext.ConnectionStringName = "KalikoCMS";
        }


        public int StartupOrder { get { return 70; } }
    }
    public class ContactStartUp : IStartupSequence
    {
        public void Startup()
        {
            var area = new ExceptionsArea();
            Dashboard.RegisterArea(area);
            AspNet.Identity.DataAccess.Data.DataContext.ConnectionStringName = "KalikoCMS";
        }


        public int StartupOrder { get { return 50; } }
    }

    public class SnippetsStartUp: IStartupSequence
    {
        public void Startup()
        {
            var area = new SnippetsArea();
            Dashboard.RegisterArea(area);
            AspNet.Identity.DataAccess.Data.DataContext.ConnectionStringName = "KalikoCMS";
        }


        public int StartupOrder { get { return 50; } }
    }


    public class HangfireStartUp : IStartupSequence
    {
        public void Startup()
        {
            var area = new HangfireArea();
            Dashboard.RegisterArea(area);
            AspNet.Identity.DataAccess.Data.DataContext.ConnectionStringName = "KalikoCMS";
        }


        public int StartupOrder { get { return 60; } }
    }

}
