﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.PageTypes
{
    public abstract class SitePage : CmsPage
    {
        [Property("Meta-description")]
        public virtual TextProperty MetaDescription { get; set; }

        [ImageProperty("Top image", Width = 1280, Height = 380)]
        public virtual ImageProperty TopImage { get; set; }
    }
}