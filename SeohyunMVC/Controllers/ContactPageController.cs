﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using KalikoCMS.Mvc.Framework;
using Newtonsoft.Json.Linq;
using SeohyunMVC.DAL;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Controllers
{

  //  [RequireHttps]
    public class ContactPageController : PageController<ContactPageType>
    {
       

        [HttpGet]
        public override ActionResult Index(ContactPageType currentPage)
        {
            var model = ContactPageViewModelBuilder.Create(currentPage);
            return View(model);
        }
       
    }


}


