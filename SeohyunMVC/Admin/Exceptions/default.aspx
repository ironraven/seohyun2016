﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="SeohyunMVC.Admin.Exceptions._default"  MasterPageFile="../Templates/MasterPages/Admin.Master"%>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <div class="admin-page">

        <div class="page-head">
            <h1>Manage Exceptions</h1>
        </div>

        <div class="main-content">
            <div class="inner-container" style="max-width: 1280px">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th style="width: 20%;">ID</th>
                            <th style="width: 25%;">EventDateTime</th>
                            <th style="width: 25%;">EventLevel</th>
                            <th style="width: 25%;">MachineName</th>
                            <th style="width: 25%;">EventMessage</th>
                            <th style="width: 15%;">ErrorSource</th>
                            <th style="width: 25%;">ErrorMessage</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%  
                            foreach (var item in Logs)
                            {
                        %>
                        <tr>
                            <td>
                                <%=item.Id %>
                            </td>
                            <td>
                                <%=item.EventDateTime %>
                            </td>
                            <td>
                                <%=item.EventLevel %>
                            </td>
                            <td>
                                <%=item.MachineName %>
                            </td>
                            <td>
                                  <%=item.EventMessage %>
                            </td>
                            <td>
                                  <%=item.ErrorSource %>
                            </td>
                            <td>
                                 <%=item.ErrorMessage %>
                            </td>
                        </tr>

                        <%  

                            }

                        %>
                        <%--<asp:Literal ID="UserList" runat="server" />--%>
                    </tbody>
                </table>
                <a href="Exceptions/Delete.aspx" class="btn btn-primary pull-right"><i class="icon-remove"></i>Delete all</a>
            </div>
        </div>

    </div>
</asp:Content>
