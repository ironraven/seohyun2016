﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace SeohyunMVC.DAL
{

    public interface IContactRepository : IDisposable
    {
        IQueryable<Models.Contact> All { get; }

        IQueryable<Models.Contact> AllIncluding(params Expression<Func<Models.Contact, object>>[] includeProperties);

        Models.Contact Find(int id);

        void InsertOrUpdate(Models.Contact contact);

        void Delete(int id);

        void Save();
    }
    public interface ISnippetRepository : IDisposable
    {
        IQueryable<Models.Snippet> All { get; }

        IQueryable<Models.Snippet> AllIncluding(params Expression<Func<Models.Snippet, object>>[] includeProperties);

        Models.Snippet Find(int id);

        void InsertOrUpdate(Models.Snippet contact);

        void Delete(int id);

        void Save();
    }

    public class SnippetsRepository : ISnippetRepository
    {
        DatabaseContext context = new DatabaseContext();
        public IQueryable<Models.Snippet> All
        {
            get { return context.Snippets; }
        }
        public IQueryable<Models.Snippet> AllIncluding(params Expression<Func<Models.Snippet, object>>[] includeProperties)
        {
            IQueryable<Models.Snippet> query = context.Snippets;

            foreach (var includeProperty in includeProperties)
            {

                query = query.Include(includeProperty);

            }

            return query;
        }

        public Models.Snippet Find(int id)

        {

            return context.Snippets.Find(id);

        }

        public void InsertOrUpdate(Models.Snippet snippet)

        {

            if (snippet.Id == default(int))
            {

                // New entity

                context.Snippets.Add(snippet);

            }
            else
            {

                // Existing entity

                context.Entry(snippet).State = EntityState.Modified;

            }

        }
        public void Delete(int id)

        {

            var snippet = context.Snippets.Find(id);

            context.Snippets.Remove(snippet);

        }

        public void Save()

        {

            context.SaveChanges();

        }

        public void Dispose()

        {

            context.Dispose();

        }
    }

    public class ContactRepository : IContactRepository
    {
        DatabaseContext context = new DatabaseContext();
        public IQueryable<Models.Contact> All
        {
            get { return context.Contacts; }
        }
        public IQueryable<Models.Contact> AllIncluding(params Expression<Func<Models.Contact, object>>[] includeProperties)
        {
            IQueryable<Models.Contact> query = context.Contacts;

            foreach (var includeProperty in includeProperties)
            {

                query = query.Include(includeProperty);

            }

            return query;
        }

        public Models.Contact Find(int id)

        {

            return context.Contacts.Find(id);

        }

        public void InsertOrUpdate(Models.Contact contact)

        {

            if (contact.Id == default(int))
            {

                // New entity

                context.Contacts.Add(contact);

            }
            else
            {

                // Existing entity

                context.Entry(contact).State = EntityState.Modified;

            }

        }
        public void Delete(int id)

        {

            var contact = context.Contacts.Find(id);

            context.Contacts.Remove(contact);

        }

        public void Save()

        {

            context.SaveChanges();

        }

        public void Dispose()

        {

            context.Dispose();

        }
    }

}