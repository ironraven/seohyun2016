﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using KalikoCMS.Search;
using SeohyunMVC.PageTypes.PropertyTypes;
using System;

namespace SeohyunMVC.PageTypes
{
   
    [PageType("ArticlePage", "Article page", PageTypeDescription = "Used for news page", AllowedTypes = new Type[] { },
        PreviewImage = "/Assets/Images/articlepage.png")]
    public class ArticlePageType: SitePage, IIndexable
    {
        
     

        [Property("Article Heading")]
        public virtual StringProperty Heading { get; set; }

        [ImageProperty("Main image", Width = 800, Height =600)]
        public virtual ImageProperty MainImage { get; set; }

        [Property("Preamble")]
        public virtual TextProperty Preamble { get; set; }

        [Property("Main body")]
        public virtual HtmlProperty MainBody { get; set; }
        [TagProperty("Tags", TagContext = "article")]
        public virtual TagProperty Tags { get; set; }

        [Property("Slides")]
        public virtual CollectionProperty<GalleryPropertyType> Slides { get; set; }

  
        public IndexItem MakeIndexItem(CmsPage page)
        {
            // We start by casting the generic CmsPage object to our page type
            var typedPage = page.ConvertToTypedPage<ArticlePageType>();

            // Get the base index item with basic information already set
            var indexItem = typedPage.GetBaseIndexItem();

            // Add additional information to index, this is where you add the page's properties that should be searchable
            indexItem.Title = typedPage.Heading.Value;
            indexItem.Summary = typedPage.Preamble.Value;
            indexItem.Content = typedPage.Preamble.Value + " " + typedPage.MainBody.Value;
            indexItem.Tags = typedPage.Tags.ToString();

            // We set a category in order to be able to single out search hits
            indexItem.Category = "Article";

            return indexItem;
        }
    }
}