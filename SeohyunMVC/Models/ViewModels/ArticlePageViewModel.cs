﻿using KalikoCMS.Core;
using KalikoCMS.Search;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models.ViewModels
{
    public class ArticlePageViewModel : IPageViewModel<ArticlePageType>
    {
        public ArticlePageViewModel(ArticlePageType currentPage)
        {
            CurrentPage = currentPage;
        }

        public ArticlePageType CurrentPage { get; private set; }
        public IEnumerable<CmsPage> TopMenu { get; set; }
        public SearchResult RelatedNews { get; set; }
    }
}