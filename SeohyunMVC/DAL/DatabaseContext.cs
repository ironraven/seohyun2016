﻿using SeohyunMVC.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SeohyunMVC.DAL
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext() : base("SeohyunContext")
        {

        }
        public DbSet<Models.Contact> Contacts { get; set; }
        public DbSet<Models.Cms_logs> Cms_Logs { get; set; }

        public DbSet<Models.Snippet> Snippets { get; set; }

        public DbSet<Models.Newsletter> Newsletter { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DatabaseContext, Configuration>());
            base.OnModelCreating(modelBuilder);
        }
    }
}