﻿using SeohyunMVC.DAL;
using SeohyunMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Generic;

namespace SeohyunMVC.Controllers
{
    public class SeoController : Controller
    {
       
        public ActionResult CanonicalTag()
        {

            var url = Request.Url.GetLeftPart(UriPartial.Path);
            return View("CanonicalTag", new SEOCanonical() { Url = url });
        }


        public ActionResult MetaDescription(Page page)
        {
            return View("MetaDescription", new SEODescription() { Text = "" });
        }
    }
}