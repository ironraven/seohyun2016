﻿using System.Collections.Generic;
using System.Linq;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.ViewModels
{
    public class ContentPageViewModelBuilder
    {
        public static ContentPageViewModel Create(ContentPageType currentPage)
        {
            var model = new ContentPageViewModel(currentPage);
            PageViewModelBuilder.SetBaseProperties(model);
           
            return model;
        }
    }
}

