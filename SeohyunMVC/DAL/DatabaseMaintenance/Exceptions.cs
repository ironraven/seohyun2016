﻿using NLog;
using SeohyunMVC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SeohyunMVC.DAL.DatabaseMaintenance
{
    public class Exceptions
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();
        // GET: Exceptions

        public static void ExecuteCleanProcedure()
        {
            try
            {
                using (var db = new DatabaseContext())
                {

                    db.Cms_Logs.RemoveRange(db.Cms_Logs.Where(x => x.EventLevel == "Debug"));
                    db.SaveChanges();
                    db.Database.Connection.Close();
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Something went Wrong:" + e.ToString());
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }
        public static void ExecuteCleanProcedureAll()
        {
            try
            {
                using (var db = new DatabaseContext())
                {

                    db.Cms_Logs.RemoveRange(db.Cms_Logs.Where(x => x.EventLevel == "Debug" || x.EventLevel == "Info" || x.EventLevel == "Error"));
                    db.SaveChanges();
                    db.Database.Connection.Close();
                }
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, "Something went Wrong:" + e.ToString());
                //return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

        }
    }
}

