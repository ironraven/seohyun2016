﻿using KalikoCMS.Attributes;
using KalikoCMS.PropertyType;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models
{
    [Serializable]
    public class Snippet
    {
        [Key]
        public int Id { get; set; }

        public string Title { get; set; }

       
        public string  Area { get; set; }

        public bool Active { get; set; }
    }
}
