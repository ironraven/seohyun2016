﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using SeohyunMVC.DAL;
using GoogleRecaptchaMvc;


namespace SeohyunMVC.Controllers
{
    //[RequireHttps]
    public class ContactController : Controller 
    {

        [HttpGet]
        public ActionResult Contact(string view)
        {
            ViewData["SaveSuccess"] = false;

            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "1", Value = "info@taeyeon.eu" });
            //using (var db = new DatabaseContext())
            // {
            //     ViewData["Departments"] = db.ContactDepartMents.Select(x => new SelectListItem
            //     {
            //         Text = x.Name,
            //         Value = x.ID.ToString(CultureInfo.InvariantCulture)
            //     }).ToList();
            // }

            ViewData["Departments"] = items.ToList();
            return View(view);
        }

        private readonly DatabaseContext _database = new DatabaseContext();
        

        [HttpPost]
      //  [CaptchaValidator]
        public ActionResult Contact(string view, Models.Contact model, bool captchaValid)
        {
            ViewData["SaveSuccess"] = false;
            if (ModelState.IsValid)
            {
                model.IP = Request.UserHostAddress;
                model.Datesumbitted = DateTime.Now;
                // ViewData["SaveSuccess"] = true;
                if (Save(model))
                {
                    ViewData["SaveSuccess"] = true;
                    ViewData["Nothing"] = false;
                    SendEmail(model);
                }
                else
                {
                    ViewData["SaveSuccess"] = false;
                    ViewData["Nothing"] = true;
                }

            }
            return View(view, model);
        }

        private bool Save(Models.Contact model)
        {
            try
            {
                return SaveIntoDb(model);
                

            }
            catch (Exception e)
            {
                //  Logger.Error(e.ToString());
                return false;
            }
        }


        private static bool SaveIntoDb(Models.Contact model)
        {
           
            using (var db = new DatabaseContext())
            {
                db.Contacts.Add(model);
               

                db.SaveChanges();
            }


            return true;
        }

        private void SendEmail(Models.Contact model)
        {

            var mail = new MailMessage { From = new MailAddress("website@Seohyun-Eu.info", model.Name) };
            mail.To.Add("postmaster@seohyun-eu.info");
            mail.Subject = "Φόρμα επικοινωνίας από Seohyun-eu.info";
            string Title = mail.Subject;
            StringBuilder builder = new StringBuilder();
            builder.AppendFormat("Name:{0}", model.Name).AppendLine("<br/>");
            builder.AppendFormat("Surname:{0}", model.Surname).AppendLine("<br/>");
            builder.AppendFormat("Telephone:{0}", model.Telephone).AppendLine("<br/>");
            builder.AppendFormat("Email:{0}", model.Email).AppendLine("<br/>");
            builder.AppendFormat("Message:{0}", model.Message).AppendLine("<br/>");
            builder.AppendFormat("Ip:{0}", model.IP).AppendLine("<br/>");
            builder.AppendFormat("Date:{0}", model.Datesumbitted).AppendLine("<br/>");
            string DynamicText = builder.ToString();
            string Body = "<!DOCTYPE html><html lang='el' xmlns='http://www.w3.org/1999/xhtml'><head><meta charset='utf-8' /><title>" + Title + "</title></head><body><table width='100%' height='100%'><tbody><tr><td><div style='float:left;width:100%'>" + DynamicText + "</div></td></tr></table></body></html>";
            mail.Body = Body;
            mail.IsBodyHtml = true;
            var smtp = new SmtpClient { EnableSsl = false };
            //Object userState = mail;


            try
            {
                //Send the email asynchronously doesnt work :x
                //await 
                //smtp.SendCompleted += new SendCompletedEventHandler(MailDeliveryComplete);
                // await smtp.SendMailAsync(mail);
                smtp.Send(mail);
                smtp.Dispose();

                //_database.Errors.Add(new Models.Error
                //{
                //    ErrorMessage = "EmailSendSuccesfully",
                //    Status = "OK",
                //    TimeOccured = DateTime.Now
                //});
                //_database.SaveChanges();
                // log.Error(string.Format("Exception while refreshing: {0}", "EmailSendSuccesfully"));
                //smtp.SendMailAsync(mail);
                // smtp.SendAsync(mail, userState);
            }
            catch (SmtpException smtpEx)
            {
                //Error handling here
                var text = smtpEx.ToString();
                //_database.Errors.Add(new Models.Error
                //{
                //    ErrorMessage = text,
                //    Status = "Error",
                //    TimeOccured = DateTime.Now
                //});
               // _database.SaveChanges();
            }
            catch (Exception ex)
            {
                var text = ex.ToString();
                //_database.Errors.Add(new Models.Error
                //{
                //    ErrorMessage = text,
                //    Status = "Error",
                //    TimeOccured = DateTime.Now
                //});

            }
        }
    }
}


