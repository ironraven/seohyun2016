﻿using System.Collections.Generic;
using KalikoCMS.Core;
using SeohyunMVC.PageTypes;
using System.Web.WebPages;


namespace SeohyunMVC.Models.ViewModels
{
    public class ContactPageViewModel : IPageViewModel<ContactPageType>
    {
        public ContactPageViewModel(ContactPageType currentPage)
        {
            CurrentPage = currentPage;
        }
        public ContactPageType CurrentPage { get; private set; }

        public IEnumerable<CmsPage> TopMenu { get; set; }
    }
}


