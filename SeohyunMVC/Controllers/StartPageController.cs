﻿using Hangfire;
using KalikoCMS;
using KalikoCMS.Mvc.Framework;
using NLog;
using SeohyunMVC.DAL;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.WebPages;

namespace SeohyunMVC.Controllers
{
  //  [RequireHttps]
    public class StartPageController : PageController<HomePageType>
    {
        private static NLog.Logger logger = LogManager.GetCurrentClassLogger();

        public override ActionResult Index(HomePageType currentPage)
        {
            var model = StartPageViewModelBuilder.Create(currentPage);
         //   logger.Info("You have visited the About view");
         //   logger.Log(LogLevel.Error, "Sample informational message");
          //  BackgroundJob.Enqueue(() => Console.WriteLine("Fire-and-forget"));
            return View(model);
            // return View();
        }

    }
}