﻿using KalikoCMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Admin.Snippets

{
    public class SnippetsArea : IDashboardArea
    {
        public string Title { get { return "TextHtml Snippets"; } }
        public string MenuName { get { return "Snippets"; } }
        public string Icon { get { return "icon-file"; } }
        public string Path
        {
            get { return "Snippets"; }

        }
    }
}