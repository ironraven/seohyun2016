﻿using KalikoCMS;
using KalikoCMS.Configuration;
using KalikoCMS.Core;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.ViewModels
{
    public class PageViewModelBuilder
    {
        public static PageViewModel<T> Create<T>(T currentPage) where T : SitePage
        {
            var model = new PageViewModel<T>(currentPage);

            SetBaseProperties(model);

            return model;
        }

        public static void SetBaseProperties(IPageViewModel<SitePage> model)
        {
            model.TopMenu = PageFactory.GetChildrenForPage(SiteSettings.RootPage, x => x.VisibleInMenu);

        }
    }
}