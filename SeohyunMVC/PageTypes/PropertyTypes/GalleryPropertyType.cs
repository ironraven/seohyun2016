﻿using KalikoCMS.Attributes;
using KalikoCMS.PropertyType;

namespace SeohyunMVC.PageTypes.PropertyTypes
{
    [PropertyType("6033A828-B49A-4A19-9C20-0F9BEBBD3273", "GalleryItem", "GalleryItem", EditorControl)]
    public class GalleryPropertyType : CompositeProperty
    {
        [Property("Header")]
        public StringProperty Header { get; set; }

        [Property("Featured link")]
        public LinkProperty Url { get; set; }

        [ImageProperty("Image", Width = 960, Height = 480)]
        public virtual ImageProperty SlideImage { get; set; }

        // Override Preview with how to render items of this type in lists.
        // It's also possible to use more complex HTML-layout here if wanted.
        public override string Preview
        {
            get { return Header.Preview; }
        }
    }
}


