﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="SeohyunMVC.Admin.Contact.Default" MasterPageFile="../Templates/MasterPages/Admin.Master" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <link rel="stylesheet" href="/content/bootstrap.min.css" />
    <link rel="stylesheet" href="/Admin/Assets/themes/dakota/theme.min.css" />
    <link rel="stylesheet" href="/content/bootstrap-datetimepicker.min.css" />

    <div class="admin-page">

        <div class="page-head">
            <h1>Manage Contact Submissions</h1>
        </div>

        <div class="main-content">
            <div class="inner-container" style="max-width: 1280px;">
                <% SeohyunMVC.Helpers.MVCUtility.RenderAction("Contacts", "List", new { }); %>

                <a href="Contact/CreateContact.aspx" class="btn btn-primary pull-right"><i class="icon-plus"></i>Add Contact Submission</a>
            </div>
        </div>

    </div>
</asp:Content>
