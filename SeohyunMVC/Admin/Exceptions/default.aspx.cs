﻿using SeohyunMVC.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SeohyunMVC.Admin.Exceptions
{
    public partial class _default : System.Web.Mvc.ViewPage
    {
        protected Literal litContactDetail;
        protected HyperLink linkAddContact;
        private readonly DatabaseContext _database = new DatabaseContext();
        public IEnumerable<Models.Cms_logs> Logs;

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            using (var db = new DatabaseContext())
            {
                Logs = db.Cms_Logs.OrderByDescending(x => x.Id).ThenByDescending(x=>x.EventDateTime).ToArray();
                ViewData["Logs"] = Logs;

            }
        }
    }
}