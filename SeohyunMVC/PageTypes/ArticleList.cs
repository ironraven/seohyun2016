﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using KalikoCMS.PropertyType;

namespace SeohyunMVC.PageTypes
{
    [PageType("ArticleList", "Article list",
       PageTypeDescription = "Used for news archives",
       AllowedTypes = new[] { typeof(ArticleList), typeof(ArticlePageType) },
       DefaultChildSortOrder = SortOrder.CreatedDate,
       DefaultChildSortDirection = SortDirection.Descending,
       PreviewImage = "/Assets/Images/newslistpage.png")]
    public class ArticleList: SitePage
    {
        
    }
}