﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using KalikoCMS.Search;
using SeohyunMVC.Business.PropertyTypes;
using SeohyunMVC.PageTypes.PropertyTypes;

namespace SeohyunMVC.PageTypes
{
    [PageType("ContactPage", "Contact page", PageTypeDescription = "Used for Contact page", PreviewImage = "/Assets/Images/articlepage.png")]
    public class ContactPageType : SitePage, IIndexable
    {
        [Property("Heading")]
        public virtual StringProperty Heading { get; set; }


        [Property("Header")]
        public virtual TextProperty Header { get; set; }

        [Property("Main body")]
        public virtual HtmlProperty MainBody { get; set; }
        [TagProperty("Tags", TagContext = "article")]
        public virtual TagProperty Tags { get; set; }


        public IndexItem MakeIndexItem(CmsPage page)
        {
            // We start by casting the generic CmsPage object to our page type
            var typedPage = page.ConvertToTypedPage<ContactPageType>();

            // Get the base index item with basic information already set
            var indexItem = typedPage.GetBaseIndexItem();

            // Add additional information to index, this is where you add the page's properties that should be searchable
            indexItem.Title = typedPage.Heading.Value;
            indexItem.Content = typedPage.MainBody.Value;
            indexItem.Tags = typedPage.Tags.ToString();

            // We set a category in order to be able to single out search hits
            indexItem.Category = "Contact";

            return indexItem;
        }
    }
}

