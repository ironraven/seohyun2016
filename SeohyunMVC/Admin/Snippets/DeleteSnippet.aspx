﻿<%@ Page Language="C#" MasterPageFile="~/Admin/Templates/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="DeleteSnippet.aspx.cs" Inherits="SeohyunMVC.Admin.Snippets.DeleteSnippet" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <link rel="stylesheet" href="/content/bootstrap.min.css" />
    <link rel="stylesheet" href="/Admin/Assets/themes/dakota/theme.min.css" />
    

    <div class="admin-page">
        <div class="page-head">
            <h1>Delete Snippet</h1>
        </div>

        <div class="main-content">
            <div class="inner-container form-horizontal">
                <% SeohyunMVC.Helpers.MVCUtility.RenderAction("Snippets", "Delete", new {id= Request.QueryString["id"]}); %>
                <div class="form-buttons">
                    <asp:LinkButton ID="SaveButton" CssClass="btn btn-primary" runat="server" OnClick="SaveButton_click">
                        <i class="icon-thumbs-up"></i> Edit Snippet
                    </asp:LinkButton>
                    <a href="Snippets" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftRegion" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightRegion" runat="server">
</asp:Content>

