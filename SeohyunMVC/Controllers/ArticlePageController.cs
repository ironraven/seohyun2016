﻿using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Mvc.Framework;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SeohyunMVC.Controllers
{
   // [RequireHttps]
    public class ArticlePageController : PageController<ArticlePageType>
    {
        [HttpGet]
        public override ActionResult Index(ArticlePageType currentPage)
        {
            var model = ArticlePageViewModelBuilder.Create(currentPage);
            
            return View(model);
        }

     
    }
}