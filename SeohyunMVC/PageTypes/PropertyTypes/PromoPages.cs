﻿using KalikoCMS.Attributes;
using KalikoCMS.PropertyType;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.PageTypes.PropertyTypes
{
    [PropertyType("8033A828-B49A-4A19-9C20-0F9BEBBD3273", "Feature", "Feature", EditorControl)]
    public class PromoPagesProperty : CompositeProperty
    {
        [Property("Promo link")]
        public LinkProperty Url { get; set; }

      


    }
}

