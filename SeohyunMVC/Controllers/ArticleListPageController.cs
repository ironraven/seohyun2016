﻿using Kaliko.ImageLibrary;
using Kaliko.ImageLibrary.Scaling;
using KalikoCMS;
using KalikoCMS.Mvc.Framework;
using SeohyunMVC.Models.ViewModels;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SeohyunMVC.Controllers
{
  //  [RequireHttps]
    public class ArticleListPageController : PageController<ArticleList>
    {
        public ActionResult Index(ArticleList currentPage, int page = 1)
        {
            var model = ArticleListPageViewModelBuilder.Create(currentPage, page);
            var pageType = KalikoCMS.Core.PageType.GetPageType(typeof(ArticlePageType));
            // Get all children from the news root
           
            foreach (var pages in model.News)
            {
                if (!string.IsNullOrEmpty(pages.TopImage.ImageUrl))
                {
                    string Image = pages.TopImage.ImageUrl.Split('/').Last();
                    var lastPart = Image.LastIndexOf(".");
                    var image = Image.Substring(0, lastPart);
                    //var Exists = string.Format("http://{0}:45801/ImageCache/cropped/{1}", Request.Url.Host, page.PageId + ".jpg");
                    if (!System.IO.File.Exists(string.Format("/ImageCache/cropped/{0}.jpg", image)))
                    {
                        var image2Init = HttpContext.Server.MapPath(pages.TopImage.OriginalImageUrl);
                        var NewImage = new KalikoImage(image2Init);
                        NewImage = NewImage.Scale(new CropScaling(650, 350));
                        string path = HttpContext.Server.MapPath("~/ImageCache/cropped");
                        path = string.Format("{0}\\{1}{2}", path, image, ".jpg");
                        NewImage.SaveJpg(@path, 65);
                       // pages.thumb.Value= string.Format("/ImageCache/cropped/{0}.jpg", image);
                    }
                }
            }


            return View(model);
        }

        [NonAction]
        public override ActionResult Index(ArticleList currentPage)
        {
            // Decorated with NonAction in order to use optional parameters
            throw new NotImplementedException();
        }
    }
}