using Hangfire;
using Hangfire.Dashboard;
using Microsoft.Owin;
using Owin;
using System;
using System.Web;

[assembly: OwinStartup(typeof(SeohyunMVC.Startup))]
namespace SeohyunMVC
{

    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);


            GlobalConfiguration.Configuration.UseSqlServerStorage("SeohyunContext");
            //jobs
            RecurringJob.AddOrUpdate(() => DAL.DatabaseMaintenance.Exceptions.ExecuteCleanProcedure(), Cron.Hourly);
            RecurringJob.AddOrUpdate(() => DAL.DatabaseMaintenance.Exceptions.ExecuteCleanProcedureAll(), Cron.Weekly);
            var options = new DashboardOptions
            {
                AppPath = VirtualPathUtility.ToAbsolute("/admin"),
                Authorization = new[] { new HangFireAuthorizationFilter() }

            };
            app.UseHangfireServer();
            app.UseHangfireDashboard("/Admin/Hangfire", options);



        }
    }
}