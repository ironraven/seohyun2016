﻿using KalikoCMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Admin.Register
{
    public class HangfireArea : IDashboardArea
    {
        public string Title { get { return "Hangfire"; } }
        public string MenuName { get { return "Hangfire"; } }
        public string Icon { get { return "icon-file"; } }
        public string Path { get { return "Hangfire"; } }
    }
}

