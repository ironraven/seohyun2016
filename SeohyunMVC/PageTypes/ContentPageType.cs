﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.PropertyType;
using KalikoCMS.Search;
using SeohyunMVC.Business.PropertyTypes;
using SeohyunMVC.PageTypes.PropertyTypes;
using SeohyunMVC.PageTypes;
using System.ComponentModel.DataAnnotations.Schema;

namespace SeohyunMVC.PageTypes
{
      
    [PageType("ContentPage", "Content page", PageTypeDescription = "Used for Content page", PreviewImage = "/Assets/Images/articlepage.png")]
    public class ContentPageType : SitePage, IIndexable
    {
        public virtual StringProperty Heading { get; set; }

        //hidden
        [Column("Images")]
        public virtual TextProperty ThumbImage { get; set; }


        [Property("Header")]
        public virtual TextProperty Header { get; set; }

        [Property("Preamble")]
        public virtual TextProperty Preamble { get; set; }

        [Property("Main body")]
        public virtual HtmlProperty MainBody { get; set; }

        [TagProperty("Tags", TagContext = "Content")]
        public virtual TagProperty Tags { get; set; }

        [Property("Slides")]
        public virtual CollectionProperty<GalleryPropertyType> Slides { get; set; }

        public IndexItem MakeIndexItem(CmsPage page)
        {
            // We start by casting the generic CmsPage object to our page type
            var typedPage = page.ConvertToTypedPage<ContentPageType>();

            // Get the base index item with basic information already set
            var indexItem = typedPage.GetBaseIndexItem();

            // Add additional information to index, this is where you add the page's properties that should be searchable
            indexItem.Title = typedPage.PageName;
            indexItem.Summary = typedPage.Preamble.Value;
            indexItem.Content = typedPage.Preamble.Value + " " + typedPage.MainBody.Value;
            indexItem.Tags = typedPage.Tags.ToString();

            // We set a category in order to be able to single out search hits
            indexItem.Category = "Content";

            return indexItem;
        }
    }
}


