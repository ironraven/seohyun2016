﻿using System.Collections.Generic;
using System.Linq;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using SeohyunMVC.PageTypes;
namespace SeohyunMVC.Models.ViewModels
{
    public class ArticlePageViewModelBuilder
    {
        public static ArticlePageViewModel Create(ArticlePageType currentPage)
        {
            var model = new ArticlePageViewModel(currentPage);
            PageViewModelBuilder.SetBaseProperties(model);

            return model;
        }
    }
}
