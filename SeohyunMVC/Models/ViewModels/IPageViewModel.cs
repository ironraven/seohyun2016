﻿using KalikoCMS.Core;
using SeohyunMVC.PageTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SeohyunMVC.Models.ViewModels
{
    public interface IPageViewModel<out T> where T : SitePage
    {
        T CurrentPage { get; }
        IEnumerable<CmsPage> TopMenu { get; set; }
    }
}