﻿using KalikoCMS.Attributes;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.Pages
{
    [PageType("ContentList", "Content list",
      PageTypeDescription = "Used for news archives",
      AllowedTypes = new[] { typeof(ContentListPages), typeof(ContentPageType) },
      DefaultChildSortOrder = SortOrder.SortIndex,
      DefaultChildSortDirection = SortDirection.Ascending,
      PreviewImage = "/Assets/Images/newslistpage.png")]
    public class ContentListPages : CmsPage
    {
    }
}