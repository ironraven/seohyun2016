﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/Templates/MasterPages/Admin.Master" AutoEventWireup="true" CodeBehind="DeleteContact.aspx.cs" Inherits="SeohyunMVC.Admin.Contact.DeleteContact" %>

<asp:Content ContentPlaceHolderID="FullRegion" runat="server">
    <link rel="stylesheet" href="/content/bootstrap.min.css" />
    <link rel="stylesheet" href="/Admin/Assets/themes/dakota/theme.min.css" />
    <link rel="stylesheet" href="/content/bootstrap-datetimepicker.min.css" />
    <script src="/scripts/Moment.js"></script>
    <script src="/scripts/bootstrap-datetimepicker.min.js"></script>
   

    <script type="text/javascript">
        $(function () {
            // target only the input in this editor template
            $('#Datesumbitted ,#datePicker').datetimepicker({
                format: 'YYYY-MM-DDTHH:mm:ssZZ',
                sideBySide: true,
            });

            function getUTCDate() {
                return moment($('#Datesumbitted').val()).utc().format('YYYY-MM-DDTHH:mm:ssZZ');
            }
        });
    </script>

    <div class="admin-page">
        <div class="page-head">
            <h1>Delete Contact Submission</h1>
        </div>

        <div class="main-content">
            <div class="inner-container form-horizontal">
                <% SeohyunMVC.Helpers.MVCUtility.RenderAction("Contacts", "Delete", new {id= Request.QueryString["id"]}); %>
                <div class="form-buttons">
                    <asp:LinkButton ID="SaveButton" CssClass="btn btn-primary" runat="server" OnClick="SaveButton_click">
                        <i class="icon-thumbs-up"></i> Edit Contact
                    </asp:LinkButton>
                    <a href="Contact/?" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="LeftRegion" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RightRegion" runat="server">
</asp:Content>
