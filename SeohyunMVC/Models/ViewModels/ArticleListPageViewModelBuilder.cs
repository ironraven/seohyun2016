﻿using System.Collections.Generic;
using System.Linq;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using PagedList;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.ViewModels
{
    public class ArticleListPageViewModelBuilder
    {
        public const int PageSize = 10;

        public static ArticleListPageViewModel Create(ArticleList currentPage, int page)
        {
            var model = new ArticleListPageViewModel(currentPage);
            PageViewModelBuilder.SetBaseProperties(model);
            model.News = new PagedList<ArticlePageType>(GetNews(currentPage), page, PageSize);
            model.NewsHolders = GetNewsHolders(currentPage);
            model.Page = page;
            return model;
        }

        private static IEnumerable<ArticlePageType> GetNews(ArticleList currentPage)
        {
            // Get the page type for news page
            var pageType = PageType.GetPageType(typeof(ArticlePageType));

            // Get all news from the list level and below
            var newsPages = PageFactory.GetPageTreeFromPage(currentPage.PageId, p => p.IsAvailable);

            // Sort on publish start date descending
            newsPages.Sort(SortOrder.StartPublishDate, SortDirection.Descending);

            // Return all news pages
            return newsPages.Where(x => x.PageTypeId == pageType.PageTypeId).Select(x => x.ConvertToTypedPage<ArticlePageType>());
        }

        private static IEnumerable<ArticleList> GetNewsHolders(ArticleList currentPage)
        {
            // Get the page type for news lists
            var pageType = PageType.GetPageType(typeof(ArticleList));

            // Get all children from the news root
            return PageFactory.GetChildrenForPageOfPageType(currentPage.RootId, pageType.PageTypeId).Select(x => x.ConvertToTypedPage<ArticleList>());
        }
    }
}