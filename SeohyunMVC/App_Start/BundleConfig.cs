﻿using BundleTransformer.Core.Builders;
using BundleTransformer.Core.Orderers;
using BundleTransformer.Core.Resolvers;
using BundleTransformer.Core.Transformers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace SeohyunMVC.App_Start
{
    public class BundleConfig
    {
        public BundleConfig()
        {
            BundleTable.EnableOptimizations = true;
          

        }
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection Bundles)
        {
            Bundles.UseCdn = true;
            var nullBuilder = new NullBuilder();
            var styleTransformer = new StyleTransformer();
            var scriptTransformer = new ScriptTransformer();
            var nullOrderer = new NullOrderer();

            // Replace a default bundle resolver in order to the debugging HTTP-handler
            // can use transformations of the corresponding bundle
            BundleResolver.Current = new CustomBundleResolver();


            Bundles.Add(new ScriptBundle("~/Bundles/jquery").Include(
                      "~/Scripts/jquery-{version}.js"));

            Bundles.Add(new ScriptBundle("~/Bundles/jqueryval").Include(
                    "~/scripts/jquery.unobtrusive-ajax.min.js",
                        // "~/Scripts/MicrosoftAjax.js",
                        //"~/Scripts/MicrosoftMvcAjax.js",
                        "~/Scripts/jquery.validate*"));

            Bundles.Add(new ScriptBundle("~/Bundles/modernizr").Include(
                     "~/Scripts/modernizr-*"));

            Bundles.Add(new ScriptBundle("~/Bundles/bootstrap").Include(
                  "~/Scripts/bootstrap.js"));

            Bundles.Add(new ScriptBundle("~/Bundles/custom").Include(
                          "~/Scripts/preload.js",
                          "~/Scripts/custom/custom.js",
                          "~/Scripts/owl.carousel.min.js",
                          "~/Scripts/jquery.easing.min.js",
                          "~/Scripts/jquery.waypoints.min.js",
                          "~/Scripts/jquery.countTo.js",
                          "~/Scripts/jquery.inview.min.js",
                          "~/Scripts/lightbox.min.js",
                          "~/Scripts/photoswipe.min.js",
                          "~/Scripts/photoswipe-ui-default.min.js",
                          "~/Scripts/wow.min.js"));

            var commonStylesBundle = (new Bundle("~/Bundles/styles").Include(
                    "~/Content/bootstrap.min.css",
                    "~/Content/owl.carousel.css",
                    "~/Content/owl.theme.css",
                    "~/Content/owl.transitions.css",
                    "~/Content/lightbox.css",
                    "~/Content/Icons/style.css",
                    "~/Content/animate.css",
                    "~/Content/photoswipe.css",
                    "~/Content/default-skin.css",
                    "~/Content/main.css",
                    "~/Content/InnerPages.css"
                    ));
            commonStylesBundle.Builder = nullBuilder;
            commonStylesBundle.Transforms.Add(styleTransformer);
            commonStylesBundle.Orderer = nullOrderer;
            Bundles.Add(commonStylesBundle);

        }

      
    }
}

