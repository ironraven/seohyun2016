﻿using System.Collections.Generic;
using System.Linq;
using KalikoCMS;
using KalikoCMS.Core;
using KalikoCMS.Core.Collections;
using SeohyunMVC.PageTypes;

namespace SeohyunMVC.Models.ViewModels
{
    public class StartPageViewModelBuilder :SitePage
    {
        public static StartPageViewModel Create(HomePageType currentPage)
        {
            var model = new StartPageViewModel(currentPage);
            PageViewModelBuilder.SetBaseProperties(model);
          //  model.LatestNews = GetLatestNews();
        
            return model;
        }
        private static IEnumerable<CmsPage> GetLatestNews()
        {
            // Get the page type based on our NewsPage definition
          //  var pageType = PageType.GetPageType(typeof(ArticlePageType));

            // Get all pages of the news page type
            var news = PageFactory.GetPages(1);

            // Sort so that the last published news is first
            news.Sort(SortOrder.StartPublishDate, SortDirection.Descending);

            // Return the five latest news
            return news.Take(6);
        }

      
      
    }
}